drop table if exists users;
drop table if exists companies;

create table users (id bigint primary key, name varchar, email varchar);
create table companies (id bigint primary key, name varchar);

DROP SEQUENCE IF EXISTS sqc_users;
CREATE SEQUENCE sqc_users START WITH 1;

DROP SEQUENCE IF EXISTS sqc_companies;
CREATE SEQUENCE sqc_companies START WITH 1;

insert into users (id, name, email) values (nextval('sqc_users'), 'test1', 'test1@foo.test');

insert into companies (id, name) values (nextval('sqc_companies'), 'company 1');
insert into companies (id, name) values (nextval('sqc_companies'), 'company 2');
insert into companies (id, name) values (nextval('sqc_companies'), 'company 3');
insert into companies (id, name) values (nextval('sqc_companies'), 'company 4');
insert into companies (id, name) values (nextval('sqc_companies'), 'company 5');
insert into companies (id, name) values (nextval('sqc_companies'), 'company 6');
insert into companies (id, name) values (nextval('sqc_companies'), 'company 7');
insert into companies (id, name) values (nextval('sqc_companies'), 'company 8');
insert into companies (id, name) values (nextval('sqc_companies'), 'company 9');
