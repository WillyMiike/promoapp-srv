
-- Enable PostGIS (includes raster)
-- CREATE EXTENSION postgis;
-- Enable Topology
-- CREATE EXTENSION  postgis_topology;

-- SELECT *
-- FROM my_table
-- WHERE ST_DWithin(gps, ST_SetSRID(ST_MakePoint(-72.657, 42.0657), 4326), 5 * 1000);

--SELECT *
--FROM companies
--WHERE ST_DWithin(location, ST_SetSRID(ST_MakePoint(-71.060316, 48.432044), 4326), 5 * 1000);


drop table if exists users;
drop table if exists companies;
drop table if exists promotions;
drop table if exists rating;
drop table if exists puntuacion;

create table users (id bigint primary key,name varchar, email varchar, password varchar);
create table companies (id bigint primary key, name varchar,direction varchar,details varchar,score int, location geography);
create table promotions (id bigint primary key, companyId int, description varchar, cost int);

DROP SEQUENCE IF EXISTS sqc_users;
CREATE SEQUENCE sqc_users START WITH 1;

DROP SEQUENCE IF EXISTS sqc_companies;
CREATE SEQUENCE sqc_companies START WITH 1;

DROP SEQUENCE IF EXISTS sqc_promotions;
CREATE SEQUENCE sqc_promotions START WITH 1;

insert into users (id, name, email, password) values (nextval('sqc_users'),'nametest', 'test1@foo.test', 'test1');

insert into companies (id, name,direction,details,score, location) values (nextval('sqc_companies'), 'La Pasiva','Av Roosevelt y Acuña de Figueroa','Pizzeria y Chiviteria','3', ST_GeomFromText('POINT(-34.94760779681668 -54.92527330054611)', 4326));
insert into companies (id, name,direction,details,score, location) values (nextval('sqc_companies'), 'La Balancita','25 de Mayo','Parrillada','4', ST_GeomFromText('POINT(-34.896378580865445 -54.94110746624562)', 4326));
insert into companies (id, name,direction,details,score, location) values (nextval('sqc_companies'), 'Sumo','Sarandi','Restaurant','4', ST_GeomFromText('POINT(-71.060316 48.432044)', 4326));
insert into companies (id, name,direction,details,score, location) values (nextval('sqc_companies'), 'Pizza Franco','Bergalli','Pizzeria','4', ST_GeomFromText('POINT(-71.060316 48.432044)', 4326));
insert into companies (id, name,direction,details,score, location) values (nextval('sqc_companies'), 'Pizza Express','Joaquin de Viana','Pizzeria','4', ST_GeomFromText('POINT(2.060316 48.432044)', 4326));
insert into companies (id, name,direction,details,score, location) values (nextval('sqc_companies'), 'Carro Don Joaquin','Joaquin de Viana','Restaurant','3', ST_GeomFromText('POINT(-34.896378580865445 -54.94110746624562)', 4326));
insert into companies (id, name,direction,details,score, location) values (nextval('sqc_companies'), 'Classic','Joaquin de Viana','Restaurant','3', ST_GeomFromText('POINT(-71.060316 48.432044)', 4326));
insert into companies (id, name,direction,details,score, location) values (nextval('sqc_companies'), 'El Poligrillo','Joaquin de Viana','Restaurant','4', ST_GeomFromText('POINT(-34.896378580865445 -54.94110746624562)', 4326));
insert into companies (id, name,direction,details,score, location) values (nextval('sqc_companies'), 'Si Querida Si','Ventura Alegre','Restaurant','4', ST_GeomFromText('POINT(-34.905412 -54.955617)', 4326));

insert into companies (id, name,direction,details,score, location) values (nextval('sqc_companies'), 'El Rey de la Hamburguesa','Avenida Paraiso','Hamburgueseria','1', ST_GeomFromText('POINT(-34.905517 -54.955523)', 4326));
insert into companies (id, name,direction,details,score, location) values (nextval('sqc_companies'), 'Finita y Crocante','Santa Teresa','Pizzeria','5', ST_GeomFromText('POINT(-34.905103 -54.955566)', 4326));
insert into companies (id, name,direction,details,score, location) values (nextval('sqc_companies'), 'Pizzeria La Barra','Santa Teresa','Pizzeria','3', ST_GeomFromText('POINT(-34.905156 -54.956188)', 4326));


insert into promotions (id, companyId, description, cost) values (nextval('sqc_promotions'), 4, '2 x 1 en Pizzas todos los Miercoles', 200);
insert into promotions (id, companyId, description, cost) values (nextval('sqc_promotions'), 4, '2 x 1 en Pizzas todos los Miercoles', 200);
insert into promotions (id, companyId, description, cost) values (nextval('sqc_promotions'), 4, '2 x 1 en Pizzas todos los Miercoles', 200);


