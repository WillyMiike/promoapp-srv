package test.cei.promoapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PromoappApplication {

	public static void main(String[] args) {
		SpringApplication.run(PromoappApplication.class, args);
	}
}
