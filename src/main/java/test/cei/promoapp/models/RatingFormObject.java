package test.cei.promoapp.models;

public class RatingFormObject {
	private Long companyId;
	private Long userId;
	private Long rating;
	
	public RatingFormObject() {
		
	}
	
	public RatingFormObject(Long companyId, Long userId, Long rating) {
		this.companyId = companyId;
		this.userId = userId;
		this.rating = rating;
	}

	public Long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getRating() {
		return rating;
	}
	public void setRating(Long rating) {
		this.rating = rating;
	}
	
	
}
