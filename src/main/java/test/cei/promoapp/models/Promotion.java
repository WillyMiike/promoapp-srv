package test.cei.promoapp.models;

public class Promotion {
	private Long id;
	private Long companyId;
	private String description;
	private int cost;
	
	
	public Promotion() {
		
	}
	
	public Promotion(Long id, Long companyId) {
		this.id = id;
		this.companyId = companyId;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getCost() {
		return cost;
	}
	public void setCost(int cost) {
		this.cost = cost;
	}
	
	
}
