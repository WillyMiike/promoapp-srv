package test.cei.promoapp.formobjects;

public class ResponseWrapper<T> {

	private Boolean success;
	private T payload;

	public ResponseWrapper(Boolean success) {
		this.success = success;
	}

	public ResponseWrapper(Boolean success, T payload) {
		this(success);
		this.payload = payload;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public T getPayload() {
		return payload;
	}

	public void setPayload(T payload) {
		this.payload = payload;
	}

}
