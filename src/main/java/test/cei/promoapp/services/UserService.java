package test.cei.promoapp.services;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import test.cei.promoapp.mappers.UserMapper;

import test.cei.promoapp.models.User;

@Service("UserService")
public class UserService {

	@Autowired
	private UserMapper userMapper;
	
	public User findUserByEmail(String email,String password) throws SQLException {
		return userMapper.findByEmail(email,password);
	}
	
	public void save(User user) throws SQLException {
		userMapper.save(user);
	}
}
