package test.cei.promoapp.services;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import test.cei.promoapp.mappers.CompanyMapper;
import test.cei.promoapp.models.Company;
import test.cei.promoapp.models.RatingFormObject;

@Service(value = "CompanyService")
public class CompanyService {

	@Autowired
	private CompanyMapper companyMapper;

	public CompanyService() {
	}

	public List<Company> getCompanies() throws SQLException {
		return companyMapper.getCompanies();
	}
	public void CreateCompany(Company company) throws SQLException {
		companyMapper.CreateCompany(company);
	}
	public List<Company> getCompaniesByProximity(float latitude, float longitude) throws SQLException {
		return companyMapper.nearCompanies(latitude, longitude);
	}

	public CompanyMapper getCompanyMapper() {
		return companyMapper;
	}

	public void setCompanyMapper(CompanyMapper companyMapper) {
		this.companyMapper = companyMapper;
	}
	
	public List<Company> nearCompanies(float latitude, float longitude) throws SQLException{
		return companyMapper.nearCompanies(latitude, longitude);
	}
	
	public Company show(Long id) throws SQLException {
		return companyMapper.show(id);
	}
	
	public void updateScore(Company company) throws SQLException {
		this.companyMapper.updateScore(company);
	}
	
}
