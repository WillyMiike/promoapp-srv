package test.cei.promoapp.services;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import test.cei.promoapp.mappers.PromotionMapper;
import test.cei.promoapp.models.Promotion;

@Service(value = "PromotionService")
public class PromotionService {
	
	@Autowired
	private PromotionMapper promotionMapper;
	
	public PromotionService() {
	}

	public List<Promotion> getPromotions(Long companyId) throws SQLException {
		return promotionMapper.getPromotions(companyId);
	}
	
	public PromotionMapper getPromotionMapper() {
		return promotionMapper;
	}

}
