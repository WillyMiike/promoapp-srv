package test.cei.promoapp.mappers;

import java.sql.SQLException;

import org.apache.ibatis.annotations.Param;

import test.cei.promoapp.models.User;

public interface UserMapper {

	public User findByEmail(@Param("email") String email,@Param("password") String password) throws SQLException;
	public void save(@Param("user") User user) throws SQLException;
}
