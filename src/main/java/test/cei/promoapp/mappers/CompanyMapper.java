package test.cei.promoapp.mappers;

import java.sql.SQLException;
import java.util.List;

import test.cei.promoapp.models.Company;
import test.cei.promoapp.models.RatingFormObject;

import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.PathVariable;

public interface CompanyMapper {

	public List<Company> getCompanies() throws SQLException;

	public void CreateCompany(@Param("company") Company company) throws SQLException;

	public List<Company> nearCompanies(@Param("latitude") Float latitude, @Param("longitude") Float longitude)
			throws SQLException;

	public Company show(@PathVariable long id) throws SQLException;
	
	public void updateScore(@Param("company") Company company) throws SQLException;

}
