package test.cei.promoapp.mappers;

import java.sql.SQLException;
import java.util.List;


import test.cei.promoapp.models.Promotion;

public interface PromotionMapper {
	public List<Promotion> getPromotions(Long companyId) throws SQLException;
	

}
