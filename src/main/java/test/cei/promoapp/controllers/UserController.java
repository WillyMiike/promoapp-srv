package test.cei.promoapp.controllers;

import java.sql.SQLException;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import test.cei.promoapp.formobjects.LoginObject;
import test.cei.promoapp.models.User;
import test.cei.promoapp.services.UserService;

@Controller
@RequestMapping(value = "/users")
public class UserController {

	@Resource(name = "UserService")
	private UserService userService;

	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody String index() {
		return "users";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public @ResponseBody User login(@RequestBody LoginObject loginObject) throws SQLException {
		User user = userService.findUserByEmail(loginObject.getEmail(),loginObject.getPassword());
		return user;
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public @ResponseBody User save(@RequestBody User user) throws SQLException {
		this.userService.save(user);
		return user;
 	}
}
