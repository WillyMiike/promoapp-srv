package test.cei.promoapp.controllers;

import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import test.cei.promoapp.models.Promotion;
import test.cei.promoapp.services.PromotionService;

@Controller
@RequestMapping(value = "/promotions")

public class PromotionController {

	

		@Resource(name = "PromotionService")
		private PromotionService promotionService;

		@RequestMapping(value = "/{companyId}", method = RequestMethod.GET)
		public @ResponseBody List<Promotion> getPromotion(@PathVariable Long companyId) throws SQLException {
			List<Promotion> promotions = null;
			try {
				promotions = this.promotionService.getPromotions(companyId);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			return promotions;
		}

}
