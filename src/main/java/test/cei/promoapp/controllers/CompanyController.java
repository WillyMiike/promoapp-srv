package test.cei.promoapp.controllers;

import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import org.springframework.web.bind.annotation.PathVariable;

import test.cei.promoapp.models.Company;
import test.cei.promoapp.models.RatingFormObject;
import test.cei.promoapp.services.CompanyService;

@Controller
@RequestMapping(value = "/companies")
public class CompanyController {

	@Resource(name = "CompanyService")
	private CompanyService companyService;

	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody List<Company> index() throws SQLException {
		List<Company> companies = null;
		try {
			companies = this.companyService.getCompanies();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return companies;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public @ResponseBody Company show(@PathVariable Long id) throws SQLException {
		System.out.println("---> " + id);
		Company c = new Company();
		c = this.companyService.show(id);
		return c;
	}

	@RequestMapping(value = "/near", method = RequestMethod.GET)
	public @ResponseBody List<Company> near(@RequestParam("latitude") float latitude,
			@RequestParam("longitude") float longitude) throws SQLException {
		System.out.println("---> " + latitude + ", " + longitude);
		List<Company> companies = null;
		companies = this.companyService.nearCompanies(latitude, longitude);
		return companies;
	}
	
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public @ResponseBody Company CreateCompany(@RequestBody Company company) throws SQLException {
		this.companyService.CreateCompany(company);
		return company;
	}
	
	@RequestMapping(value = "/{id}/rating", method = RequestMethod.POST)
	public @ResponseBody Company rating(@RequestBody RatingFormObject rating) throws SQLException {
		System.out.println("---> getRating: " + rating.getRating() + ", getCompanyId: " + rating.getCompanyId());
		Company c = new Company(rating.getCompanyId(), rating.getRating());
		this.companyService.updateScore(c);
		return c;
 	}

}
